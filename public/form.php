<!--<html>
  <head>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
  </head>
  <body>

</*?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

Далее выводим форму отмечая элементы с ошибками классом error
 и задавая начальные значения элементов ранее сохраненными.
?>*\

    <form action="" method="POST">
      <input name="fio" </?php if ($errors['fio']) {print 'class="error"';} ?> value="</?php print $values['fio']; ?>" />
      <input type="submit" value="ok" />
    </form>
  </body>
</html>-->
<html>
<head>

    <title>Форма с базой данных</title>
    <link rel="stylesheet" media="all" href="style.css"/>
    <style>
        /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
        .error {
            border: 2px solid red;
        }
        .error-radio
        {
            border: 2px solid red;
            width: 150px;
        }
        .error-bio
        {
            border: 2px solid red;
            width: 250px;
            height: 16px;
        }
        .error-check
        {
            border: 2px solid red;
            width: 350px;
        }
        .error-abil
        {
            border: 2px solid red;
            width: 290px;
            height: 70px;
        }
        .error-radio-limb
        {
            border: 2px solid red;
            width: 250px;
            height:35px;
        }
    </style>
</head>
<body>
<?php
if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>

<form action="index.php" id="my-formcarry" accept-charset="UTF-8" class="main" method="POST">
Введите имя:<br/>
    <input style="margin-bottom : 1em" id="formname" type="text" name="fio" placeholder="Введите имя"
        <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>">
 <br/>
        Введите почту:<br/>
    <input style="margin-bottom : 1em;margin-top : 1em" id="formmail" type="email" name="email"
           placeholder="Введите почту"
        <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"
    >
<br/>
    <label >
        Выберите свой год рождения:<br/>
        <input <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year_value']; ?> id="dr" name="birthyear"
        value=""
        type="number"/>
    </label><br/>

    Выберите пол:<br/>
    <label <?php if($errors['sex']){print 'class="error-radio"';}?>>
        <input <?php if($values['sex_value']=="man"){print 'checked';}?> type="radio"
                                                                         name="radio2" value="man"/>
        Мужской</label>
    <label <?php if($errors['sex']){print 'class="error-radio"';}?>> <input <?php if($values['sex_value']=="woman"){print 'checked';}?> type="radio"
                                                                                                                                        name="radio2" value="woman"/>
        Женский</label>
    <br/>
    <div <?php if($errors['limb']){print 'class="error-radio-limb"';}?>>
        Количество конечностей :<br/>
        <label><input
                <?php if($values['limb_value']=="0"){print 'checked';}?>
                    type="radio"
                    name="radio1" value="0"/>
            0</label>
        <label><input
                <?php if($values['limb_value']=="1"){print 'checked';}?>
                    type="radio"
                    name="radio1" value="1"/>
            1</label>
        <label><input
                <?php if($values['limb_value']=="2"){print 'checked';}?>
                    type="radio"
                    name="radio1" value="2"/>
            2</label>
        <label><input
                <?php if($values['limb_value']=="3"){print 'checked';}?>
                    type="radio"
                    name="radio1" value="3"/>
            3</label>
        <label><input
                <?php if($values['limb_value']=="4"){print 'checked';}?>
                    type="radio"
                    name="radio1" value="4"/>
            4</label>
        <label><input
                <?php if($values['limb_value']=="5"){print 'checked';}?>
                    type="radio"
                    name="radio1" value="5"/>
            5</label><br>
    </div>
    <label>
        Ваши сверхспособности:
        <br/>
        <div <?php if ($errors['abil']) {print 'class="error-abil"';} ?>> <select id="sp" name="select1[]"
                                                                                  multiple="multiple">
                <option  <?php if($values['abil_value']=="god"){print 'selected';}?>value="god">Бессмертие</option>
                <option  <?php if($values['abil_value']=="wall"){print 'selected';}?>value="wall">Прохожение сквозь стены</option>
                <option  <?php if($values['abil_value']=="levity"){print 'selected';}?>value="levity">Левитация</option>
                <option  <?php if($values['abil_value']=="kos"){print 'selected';}?>value="kos">Умение понимать дискретку с первого раза</option>
                <option  <?php if($values['abil_value']=="kol"){print 'selected';}?>value="kol">Умение двигать предметы силой мысли</option>

            </select> </div>
    </label><br/>

    <label <?php if ($errors['bio']) {print 'class="error-bio"';} ?>>
        Биография: <br/>
        <textarea id="biog" name="textarea1" placeholder="Пиши тут"><?php print $values['bio_value'];?></textarea>
    </label><br/>

    <div <?php if ($errors['check']) {print 'class="error-check"';} ?>><label><input <?php if($values['check_value']=="1"){print 'checked';}?> style="margin-bottom : 1em;margin-top : 1em;" id="formcheck" type="checkbox" name="checkbox"
                                                                                                                                               value="1">Согласие на обработку персональных данных</label></div>

    <input type="submit" style="margin-bottom : -1em" id="formsend" class="buttonform" value="Отправить">
</form>
</body>
</html>
